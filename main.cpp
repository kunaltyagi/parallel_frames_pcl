#include <atomic>
#include <chrono>
#include <future>
#include <iomanip>
#include <iostream>
#include <memory>
#include <mutex>
#include <sstream>
#include <string>

#include <boost/asio.hpp>
#include <boost/circular_buffer.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include <taskflow/taskflow.hpp>

using CloudPtr = std::shared_ptr<pcl::PointCloud<pcl::PointXYZ>>;
using TaskPtr = std::shared_ptr<tf::Taskflow>;

std::mutex input_mutex, output_mutex, task_mutex;
boost::circular_buffer<CloudPtr> input_q{30}, output_q{20};
TaskPtr current_flow;

const int max_threads = 8;
// ideally, separate the atomics by std::hardware_destructive_interference_size
std::atomic<bool> loop = true;
std::atomic<int> threads = 0;
std::atomic<unsigned int> counter = 0;

// get current time. Why is it so long??
auto
now()
{
  return std::chrono::duration_cast<std::chrono::microseconds>(
             std::chrono::system_clock::now().time_since_epoch())
      .count();
}

// the per-frame work that needs to be done. I'll just sleep
auto
work(CloudPtr cloudPtr, unsigned int frame)
{
  std::stringstream data;
  data << "[" << now() << "] " << frame << " Simple process start\n";
  std::cout << data.str();
  std::this_thread::sleep_for(
      std::chrono::milliseconds{static_cast<unsigned int>(200)});
  data.str("");
  data.clear();
  data << "[" << now() << "] " << frame << " Simple process end\n";
  std::cout << data.str();
  return cloudPtr;
}

// boilerplate that'll not change for one output from one input type of pipeline
template <class T = void>
auto
create_taskflow_frame(double loop_rate, std::shared_future<T> fut_in = {})
{
  auto flow = std::make_shared<tf::Taskflow>();
  tf::Task curr_task;
  CloudPtr cloud;

  std::chrono::microseconds delta_t{static_cast<unsigned int>(1000000. / loop_rate)};
  boost::asio::io_service io;
  std::lock_guard task{task_mutex};
  for (boost::asio::steady_timer t(io); loop; t.wait()) {
    t.expires_after(delta_t);
    {
      std::lock_guard inp{input_mutex};
      if (input_q.size() == 0) {
        continue;
      }
      cloud = input_q.front();
      input_q.pop_front();
    }

    curr_task =
        flow->emplace([flow, cloud, fut = std::move(fut_in)](tf::Subflow& subflow) {
          auto frame = counter.fetch_add(1);
          auto output = work(cloud, frame);
          {
            std::lock_guard out{output_mutex};
            output_q.push_back(output);
          }
          if (fut.valid()) {
            fut.wait();
          }
          threads.fetch_sub(1);
          std::cout << "" << frame << " Wait over\n";
        });
    break;
  }
  return flow;
}

int
main()
{
  // Single-threaded function to capture the data and push it into the input buffer
  auto input_func = [](double loop_rate) {
    if (loop_rate <= 0)
      [[unlikely]] { return; }

    std::chrono::microseconds delta_t{static_cast<unsigned int>(1000000. / loop_rate)};
    boost::asio::io_service io;
    for (boost::asio::steady_timer t(io); loop; t.wait()) {
      t.expires_after(delta_t);
      std::stringstream data;
      data << "[" << now() << "] Frame pushed\n";
      std::cout << data.str();
      {
        std::lock_guard g{input_mutex};
        input_q.push_back(std::make_shared<pcl::PointCloud<pcl::PointXYZ>>());
      }
    }
    std::cout << "Done producing\n";
  };
  // Single threaded function to take data from output buffer and utilize it
  auto output_func = [](double loop_rate) {
    if (loop_rate <= 0)
      [[unlikely]] { return; }
    std::chrono::microseconds delta_t{static_cast<unsigned int>(1000000. / loop_rate)};
    boost::asio::io_service io;
    for (boost::asio::steady_timer t(io); loop; t.wait()) {
      t.expires_after(delta_t);
      std::lock_guard g{output_mutex};
      if (output_q.size())
        [[likely]]
        {
          std::stringstream data;
          data << "[" << now() << "] Frame consumed\n";
          std::cout << data.str();
          output_q.pop_front();
        }
    }
    std::cout << "Done consuming\n";
  };
  // Function to take data from input buffer and push it in output buffer
  // It calls the `work` function via taskflow to maximize parallelism
  // This function is essentially a launcher of worker threads
  auto process_func = [](double loop_rate) {
    if (loop_rate <= 0)
      [[unlikely]] { return; }
    TaskPtr flow;
    bool first = true;
    std::shared_future<void> fut;
    tf::Executor ex(max_threads);

    std::chrono::microseconds delta_t{static_cast<unsigned int>(1000000. / loop_rate)};
    boost::asio::io_service io;
    for (boost::asio::steady_timer t(io); loop; t.wait()) {
      t.expires_after(delta_t);
      // count semaphores here
      bool updated = false;
      int thread_cnt;
      while (!updated) {
        thread_cnt = threads.load();
        updated = threads.compare_exchange_weak(thread_cnt, thread_cnt + 1);
      }
      if (thread_cnt >= max_threads) {
        continue;
      }
      std::stringstream data;
      data << "[" << now() << "] Creating another thread: " << thread_cnt << "\n";
      std::cout << data.str();
      flow = create_taskflow_frame(loop_rate, std::move(fut));
      fut = ex.run(*flow).share();
      first = false;
    }
    ex.wait_for_all();
    std::cout << "Done processing\n";
  };

  // start the CPU load average = 100%
  std::thread inp_t(input_func, 11);
  std::thread out_t(output_func, 8);
  std::thread proc_t(process_func, 10);
  
  // Fake user interaction to stop this program after a while
  std::this_thread::sleep_for(std::chrono::seconds{static_cast<unsigned int>(30)});
  loop = false;

  // Be smarter and use std::jthread
  inp_t.join();
  out_t.join();
  proc_t.join();
  return 0;
}
